#!/bin/bash
TOP_DIR=$(pwd)

for d in $(find . -mindepth 1 -type d)
do
    cd $d
    #echo $(pwd)
    FILES=`ls *.zip`
    for f in $FILES
    do
        #echo "Unzipping ${f}"
        unzip $f
    done
    cd $TOP_DIR
done
